var express = require ('express');
var request = require('request');
var config = require('../config.json');

var router = express.Router();

/*
 * Client ID and Client secret are retrieved from developer.atlassian.com
 */
var client_id = config.client_id;
var client_secret = config.client_secret;
var callback_url = config.callback_url;

/*
 * This displays the home page
 */
router.get('/', function(req, res, next) {
  res.render('index', {
                        title: "3LO",
                        client_id: client_id,
                        client_secret: client_secret,
                        scope: 'write:jira-work',
                        state: '12345',
                        callback: callback_url
                      });
});

/*
 * Jira calls this on successful authorization
 */
router.get('/3lo-callback', function(req, res, next) {
    let code = req.query.code;
    let access_token_options = { method: 'POST',
        url: 'https://auth.atlassian.com/oauth/token',
        headers:
            {
                'Content-Type': 'application/json'
            },
        form: { grant_type: 'authorization_code',
                code: `${code}`,
                client_id: client_id,
                client_secret: client_secret,
                redirect_uri: callback_url
              }
    };

    //Generate an access token
    request.post(access_token_options, function(response, error, body) {
        let access_token = JSON.parse(body).access_token;

        //Try to use the access_token to call REST API calls
        request.get({
                        url: 'https://api.atlassian.com/oauth/token/accessible-resources',
                        headers: {
                            Authorization: `Bearer ${access_token}`,
                            Accept: 'application/json'
                        }
                    },
                    function(response, body, error) {
                        res.render('index', {
                            title: '3LO',
                            client_id: client_id,
                            client_secret: client_secret,
                            scope: 'write:jira-work',
                            state: '12345',
                            callback: callback_url,
                            body: JSON.stringify(body, null, 2)
                        });
                    });
    });


});

module.exports = router;
